# Voice Attack for Elite Dangerous in Linux


### Disclaimer
Please note that I can't provide technical support if this method doesn't work for you. I assume no liability if following this guide results in problems with your computer. This was simply my own steps to get it working. Feel free to submit a pull request or create a new issue if there is anything in this guide that is out of date.

## What you will need:

* Protontricks this will make it easy to install everything we need into the same prefix as Elite Dangerous
* SpeechPlatformRuntime.msi. just go ahead and install it to the prefix directory (noted above). Edit: some have reported that you need to use the x86 version, not the x64
* MSSpeech_SR_en-US_TELE.msi
* VoiceAttack (You can use the trial for initial setup and testing on your own system, but you will need a full license in order to use the HCS Voicepacks)
* If you intend to use HCS voicepacks, a valid license
* Knowledge of your prefix installation directory. Default prefix installation location is

    `~/.steam/steam/steamapps/compatdata/359320/pfx/`

FYI the steam id for Elite Dangerous is **359320**, which is referenced multiple times below.

## Caveats
The voice recognition is minimal, don't expect perfect accuracy. It can handle the simpler commands like launching the ship, show side-menus, initiate autopilot, redistribute power, engage Frameshift Drive, etc. It would be better if we had a way to train the voice model, but I currently don't know of a way to do that in Linux, nor do I know of a way to export a trained model from Windows 7 (W10 model did not work, I tried)

## Instructions
All of this is installed into the wine prefix for elite dangerous

1. Install Elite: Dangerous via Steam. Launch at least once so protontricks can find it
2. Install dotnet40 via protontricks (required for ED)

    `protontricks 359320 -q dotnet40 win7`

3. Install dotnet472 via protontricks (required for VoiceAttack)

    `protontricks 359320 dotnet472`

4. Install the speech platform runtime next. first you need to cd to whatever directory you downloaded it to (i used drive_c within my steam prefix to keep everything contained)

    `cd ~/.steam/steam/steamapps/compatdata/359320/pfx/drive_c`

    `protontricks 359320 msxml3`

    `protontricks -c 'wine msiexec /i ~/.steam/steam/steamapps/compatdata/359320/pfx/drive_c/SpeechPlatformRuntime.msi' 359320`

    `protontricks -c 'wine msiexec /i ~/.steam/steam/steamapps/compatdata/359320/pfx/drive_c/MSSpeech_SR_en-US_TELE.msi' 359320`

5. You should be ready now to install VoiceAttack.

    `protontricks -c 'wine "/path/to/voiceattack/installer.exe"' 359320`

6. Launch VoiceAttack at least once before trying to install any HCS Voicepacks. **(this is the command you'll need to issue to launch VoiceAttack in the future)**

   ` protontricks -c 'WINEESYNC=1 wine ~/.steam/steam/steamapps/compatdata/359320/pfx/drive_c/Program\ Files\ \(x86\)/VoiceAttack/VoiceAttack.exe' 359320`

***Stop here if you just want VoiceAttack without HCS Voicepacks***

7. Install the HCS Voicepack (register the full version of voiceattack before proceeding)
    `protontricks 359320 winhttp`
    `protontricks -c 'wine /path/to/hcs/voicepack/installer.exe' 359320`
8. Set up your keybindings for HCS Voicepack as you see fit

That's it! If all went well, start Elite: Dangerous until you get to the launcher, then VoiceAttack, and then select play from the launcher (that order seems to work best in my experience)

## License
This instructional document is available under the MIT License. You are free to use it how you wish
